<?php

namespace Drupal\element_script_template\Element;

use Drupal\Core\Render\Element\RenderElement;

/**
 * Provides a render element for any HTML tag, with properties and value.
 *
 * Properties:
 * - #url: The tag name to output.
 *
 * Usage example:
 * @code
 * $build['hello'] = [
 *   '#type' => 'script_template',
 *   '#url' => 'p',
 * ];
 * @endcode
 *
 * @RenderElement("script_template")
 */
class ScriptTemplate extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return array(
      '#pre_render' => array(
        array($class, 'preRenderHtmlTag'),
      ),
      '#attributes' => array(),
      '#value' => NULL,
    );
  }

  /**
   * Pre-render callback: Renders a generic HTML tag with attributes into #markup.
   *
   * @param array $element
   *   An associative array containing:
   *   - #tag: The tag name to output. Typical tags added to the HTML HEAD:
   *     - meta: To provide meta information, such as a page refresh.
   *     - link: To refer to stylesheets and other contextual information.
   *     - script: To load JavaScript.
   *     The value of #tag is escaped.
   *   - #attributes: (optional) An array of HTML attributes to apply to the
   *     tag. The attributes are escaped, see \Drupal\Core\Template\Attribute.
   *   - #value: (optional) A string containing tag content, such as inline
   *     CSS. The value of #value will be XSS admin filtered if it is not safe.
   *   - #noscript: (optional) If TRUE, the markup (including any prefix or
   *     suffix) will be wrapped in a <noscript> element. (Note that passing
   *     any non-empty value here will add the <noscript> tag.)
   *
   * @return array
   */
  public static function preRenderHtmlTag($element) {
    $element['#markup'] = file_get_contents($element['#url']);
    return $element;
  }

}
